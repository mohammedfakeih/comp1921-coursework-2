#include <stdio.h>
#include "file.h"
#include "logic.h"

int inputRow = 0;
int inputCol = 0;
int timeSteps = 0;

struct Cell matrix[MaxRow][MaxCol];

int readFile (FILE* file) {
    if (file == NULL) {
        return 1;
    }

    fscanf(file, "%ix%i\n", &inputCol, &inputRow); // Dimensions of the world
    fscanf(file, "%i\n", &timeSteps); // Number of steps in time the world will go through


    printf("%i\n", timeSteps);
    printf("%i\n", inputRow);

    for(int i = 0; i < inputRow; i++) {
        for (int j = 0; j < inputCol; j++) {
            fscanf(file, "%i ", &matrix[i][j].value);
            if (matrix[i][j].value != 0 && matrix[i][j].value != 1) {
                return 1; // Invalid grid value
            }
            matrix[i][j].nextValue = matrix[i][j].value;
            matrix[i][j].x = j;
            matrix[i][j].y = i;
        }
    }
    return 0;
}

int writeFile (FILE* file) {
    if (file == NULL) {
        return 1;
    }

    fprintf(file, "%ix%i\n", inputCol, inputRow);
    fprintf(file, "%i\n", timeSteps);

    for(int i = 0; i < inputRow; i++) {
        for (int j = 0; j < inputCol; j++) {
            fprintf(file, "%i ", matrix[i][j].value);
        }
        fprintf(file, "\n");
    }

    return 0;
}