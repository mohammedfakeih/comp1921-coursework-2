#ifndef FILE_H
#define FILE_H

// Reads contents of a file and creates the world with the given settings
// Returns 0 when file is loaded successfully, and 1 if an error occurs
int readFile(FILE* file);

// Outputs the state of the current world to a file
// Returns 0 if world is saved successfully, and 1 if an error occurs
int writeFile(FILE* file);


extern int inputRow;
extern int inputCol;
extern int timeSteps;

#endif //FILE_H
