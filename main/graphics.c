#include <stdio.h>
#include <SDL2/SDL.h>
#include "graphics.h"
#include "file.h"
#include "logic.h"

// Screen dimensions
const int SCREEN_WIDTH = 1680;
const int SCREEN_HEIGHT = 970;
const int TILE_WIDTH = 10;
const int TILE_HEIGHT = 10;

SDL_Window* window = NULL;
SDL_Surface* screenSurface = NULL;
SDL_Surface* gridSurface = NULL;
SDL_Surface* tileMap = NULL;
SDL_Rect tileClips[2] = {{0, 0, 10, 10}, {0, 10, 10, 10}};
struct Tile gridTiles[13600];

// The following section of code is taken from Lazy Foo: https://lazyfoo.net/tutorials/SDL/01_hello_SDL/index2.php
// *****************************
int init () {
    // Initialize
    if (SDL_Init(SDL_INIT_VIDEO) < 0 ) {
        printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
    }

    else {
        // Create the window
        window = SDL_CreateWindow("SDL Test", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH,
                                  SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
        if (window == NULL) {
            printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
        } else {
            screenSurface = SDL_GetWindowSurface(window);
        }
    }
}
// *****************************

int loadTileMap() {
    tileMap = SDL_LoadBMP("images/tilemap.bmp");
    if (tileMap == NULL) {
        printf("Bitmap could not be loaded! SDL_Error: %s\n", SDL_GetError());
        return 1;
    }
    return 0;
}

int setUpTiles() {
    for (int i = 0; i < inputRow; i++) {
        for (int j = 0; j < inputCol; j++) {
            struct Tile currentTile = gridTiles[i * inputCol + j];
            currentTile.type = matrix[i][j].value;
            currentTile.x = j * 10;
            currentTile.y = i * 10;

            SDL_Rect tileRect = {currentTile.x, currentTile.y, TILE_WIDTH, TILE_HEIGHT};

            SDL_BlitSurface(tileMap, &tileClips[currentTile.type], screenSurface, &tileRect);
        }
    }
    SDL_UpdateWindowSurface(window);
    SDL_Delay(500);
    return 0;
}

int shutDown() {
    SDL_DestroyWindow(window);
    SDL_Quit();
    return 0;
}
