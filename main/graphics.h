#ifndef GRAPHICS_H
#define GRAPHICS_H
#include <SDL2/SDL.h>

struct Tile {
    int x;
    int y;
    int type;
};

// Initializes application
// Returns 0 if successful
int init();

// Shuts down application
// Returns 0 if successful
int shutDown();

// Loads tile map used for application
// Returns 0 if successful, and 1 if image cannot be found
int loadTileMap();

// Sets up each new generation of tiles (cells)
// Returns 0 if successful
int setUpTiles();

#endif // GRAPHICS_H
