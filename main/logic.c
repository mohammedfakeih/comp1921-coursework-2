#include <stdio.h>
#include "logic.h"
#include "file.h"

int checkCell(int x, int y) {
    if (x < 0 || x >= inputCol || y < 0 || y >= inputRow) { // Checks if coordinates are out of bound
        return 0;
    }
    return matrix[y][x].value;
}

int computeCell(struct Cell* cell) {
    if (cell == NULL || cell->x < 0 || cell->x >= inputCol || cell->y < 0 || cell->y >= inputRow) {
        return -1;
    }

    int cellCount = 0;
    // Summing up all adjacent cells' values
    cellCount += checkCell(cell->x - 1, cell->y - 1);
    cellCount += checkCell(cell->x, cell->y - 1);
    cellCount += checkCell(cell->x + 1, cell->y - 1);
    cellCount += checkCell(cell->x - 1, cell->y);
    cellCount += checkCell(cell->x + 1, cell->y);
    cellCount += checkCell(cell->x - 1, cell->y + 1);
    cellCount += checkCell(cell->x, cell->y + 1);
    cellCount += checkCell(cell->x + 1, cell->y + 1);

    if (cell->value == 0 && cellCount == 3) { // Reproduction
        return 1;
    }

    else if (cell->value == 1 && (cellCount < 2 || cellCount > 3)) { // Underpopulation and overpopulation
        return 0;
    }

    return cell->value; // Cell's value doesn't change
}

int computeGrid(int gridRows, int gridCols) {
        for (int i = 0; i < gridRows; i++) {
            for(int j = 0; j < gridCols; j++) {
                matrix[i][j].nextValue = computeCell(&matrix[i][j]);
            }
        }
        for (int i = 0; i < gridRows; i++) {
            for(int j = 0; j < gridCols; j++) {
                matrix[i][j].value = matrix[i][j].nextValue;
            }
        }
    return 0;
}