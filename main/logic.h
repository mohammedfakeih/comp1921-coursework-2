#ifndef LOGIC_H
#define LOGIC_H

// Dimensions of the largest board possible
#define MaxRow 80
#define MaxCol 170

// Definition of the struct Cell used in the Game of Life
struct Cell {
    int value;
    int nextValue;
    int x;
    int y;
};

extern struct Cell matrix[MaxRow][MaxCol]; // The matrix that contains all cells

// Returns the value of the cell with the given coordinates (0 or 1)
int checkCell(int x, int y);

// Computes the value of the cell inputted based on the adjacent cells' values
// Returns the value computed (0 or 1) and alters the value field of the cell accordingly
int computeCell(struct Cell* cell);

// Evaluates values of all cells in the grid
// Returns 0 if no errors are found and -1 if an error occurs
int computeGrid(int gridRows, int gridCols);

#endif //LOGIC_H