#include <stdio.h>
#include <SDL2/SDL.h>
#include "graphics.h"
#include "logic.h"
#include "file.h"

int main (int argc, char* args[]) {
    FILE* infile = fopen("input.txt", "r");
    printf("%i", readFile(infile));
    fclose(infile);

    init();

    loadTileMap();

    setUpTiles();

    for (int t = 0; t < timeSteps; t++) {
        computeGrid(inputRow, inputCol);
        setUpTiles();
    }

    FILE* outfile = fopen("output.txt", "w");
    writeFile(outfile);
    fclose(outfile);

    shutDown();

    return 0;
}