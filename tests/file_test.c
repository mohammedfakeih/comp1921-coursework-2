#include <stdio.h>
#include <unity.h>
#include "logic.h"
#include "file.h"

void setUp (void) {};
void tearDown (void) {};

void test_normalValues_readFile() {
    FILE* file = fopen("../main/input.txt", "r");
    TEST_ASSERT_EQUAL(0, readFile(file));
    TEST_ASSERT_EQUAL(4, inputRow);
    TEST_ASSERT_EQUAL(4, inputCol);
    TEST_ASSERT_EQUAL(1, timeSteps);
    fclose(file);
}

void test_invalidValues_readFile() {
    FILE* file = fopen("../main/badInput.txt", "r");
    TEST_ASSERT_EQUAL(1, readFile(file));
}

void test_nullPointer_readFile() {
    TEST_ASSERT_EQUAL(1, readFile(NULL));
}

void test_normalValues_writeFile() {
    FILE* file = fopen("../main/output.txt", "w");
    TEST_ASSERT_EQUAL(0, writeFile(file));
    fclose(file);
}

void test_nullPointer_writefile() {
    TEST_ASSERT_EQUAL(1, writeFile(NULL));
}

int main (void) {
    UNITY_BEGIN();
    RUN_TEST(test_normalValues_readFile);
    RUN_TEST(test_normalValues_writeFile);
    RUN_TEST(test_nullPointer_readFile);
    RUN_TEST(test_nullPointer_writefile);
    RUN_TEST(test_invalidValues_readFile);
    return UNITY_END();
}
