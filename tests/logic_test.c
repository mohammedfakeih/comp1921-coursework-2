#include <stdio.h>
#include <unity.h>
#include "logic.h"
#include "file.h"

void setUp (void) {};
void tearDown (void) {};

void test_normalValues_checkCell() {
    inputRow = 2;
    inputCol = 4;
    matrix[1][3].value = 0;
    matrix[0][1].value = 1;
    TEST_ASSERT_EQUAL(0, checkCell(3, 1));
    TEST_ASSERT_EQUAL(1, checkCell(1, 0));
}

void test_invalidValues_checkCell() {
    inputRow = 3;
    inputCol = 3;
    TEST_ASSERT_EQUAL(0, checkCell(-2, 1));
    TEST_ASSERT_EQUAL(0, checkCell(0, 6));
}

void test_normalValues_computeCell() {
    inputRow = 4;
    inputCol = 3;

    struct Cell cellA = {1, 1, 1, 1};
    struct Cell cellB = {0, 0, 1, 0};

    matrix[0][0].value = 1;
    matrix[0][1].value = 0;
    matrix[0][2].value = 1;
    matrix[1][0].value = 0;
    matrix[1][1].value = 1;
    matrix[1][2].value = 0;
    matrix[2][0].value = 1;
    matrix[2][1].value = 0;
    matrix[2][2].value = 0;
    TEST_ASSERT_EQUAL(1, computeCell(&cellA));
    TEST_ASSERT_EQUAL(1, computeCell(&cellB));
}

void test_nullPointer_computeCell() {
    TEST_ASSERT_EQUAL(-1, computeCell(0));
}

void test_invalidValues_computeCell() {
    inputRow = 3;
    inputCol = 2;

    struct Cell invalidCell = {1, -5, 43};
    TEST_ASSERT_EQUAL(-1, computeCell(&invalidCell));
}

void test_validValues_computeGrid() {
    matrix[1][0].value = 1;
    matrix[0][1].value = 1;
    matrix[1][2].value = 1;
    TEST_ASSERT_EQUAL(0, computeGrid(3, 3, 1));
    TEST_ASSERT_EQUAL(1, matrix[1][1].value);
    TEST_ASSERT_EQUAL(1, matrix[0][1].value);
}

int main(void) {
    UNITY_BEGIN();
    RUN_TEST(test_normalValues_checkCell);
    RUN_TEST(test_invalidValues_checkCell);
    RUN_TEST(test_normalValues_computeCell);
    RUN_TEST(test_nullPointer_computeCell);
    RUN_TEST(test_invalidValues_computeCell);
    RUN_TEST(test_validValues_computeGrid);
    return UNITY_END();
}